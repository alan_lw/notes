# 支付宝相关：

支付宝个人收款码生成：

    alipays://platformapi/startapp?appId=20000123&actionType=scan&biz_data={"s": "money","u": "2088302202671762","a": "0.01","m": "4073000002"}

u是  https://openhome.alipay.com/platform/keyManage.htm?keyType=partner 这里的pid, 
m是：订单中的收款理由

# 支付宝内部功能调用APP的said说明

> 参考 https://my.oschina.net/xinyu126/blog/2963198

支付宝收款码

[alipayqr://platformapi/startapp?saId=20000](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000007)123

微信扫一扫
[weixin://scanqrcode](https://link.jianshu.com/?t=weixin://scanqrcode)
（跳转微信扫一扫）

支付宝扫一扫
[alipayqr://platformapi/startapp?saId=10000007](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000007)
（跳转支付宝扫一扫）

追加：

如果希望扫一扫和二维码集成，点击一个按钮，立即就跳出支付页面，则使用如下：

````
alipayqr://platformapi/startapp?saId=10000007&qrcode=HTTPS%3a%2f%2fQR.ALIPAY.COM%2fFKX09099VQZDCJ1QFGXA9F
````

这里的二维码需要使用urlencode进行编码，否则不能用哦



![img](./alipay.assets\22043-20180307083850465-1934354217.png)


支付宝付款
[alipay://platformapi/startapp?appId=20000056](https://link.jianshu.com/?t=alipay://platformapi/startapp?appId=20000056)
（跳转支付宝转账向商家付款界面）

支付宝记账
[alipay://platformapi/startapp?appId=20000168](https://link.jianshu.com/?t=alipay://platformapi/startapp?appId=20000168)
（跳转支付宝记账界面）

支付宝滴滴
[alipay://platformapi/startapp?appId=20000778](https://link.jianshu.com/?t=alipay://platformapi/startapp?appId=20000778)

支付宝蚂蚁森林
[alipay://platformapi/startapp?appId=60000002](https://link.jianshu.com/?t=alipay://platformapi/startapp?appId=60000002)

支付宝转账
[alipayqr://platformapi/startapp?saId=20000116](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=20000116)
（跳转支付宝转账界面）

支付宝手机充值
[alipayqr://platformapi/startapp?saId=10000003](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)
（跳转支付宝手机充值页面）


支付宝卡包
[alipayqr://platformapi/startapp?saId=200000](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)21
（跳转支付宝卡包页面）

支付宝吱口令
[alipayqr://platformapi/startapp?saId=200000](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)85
（跳转支付宝吱口令页面）

支付宝芝麻信用
[alipayqr://platformapi/startapp?saId=20000](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)118
（跳转支付宝芝麻信用页面）

支付宝红包
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)88886666
（跳转支付宝红包页面）

支付宝爱心
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)1000009
（跳转支付宝献爱心页面）

支付宝升级页面
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)2000066
（跳转支付宝升级页面）

支付宝滴滴打的
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)2000130
（跳转支付宝滴滴打的页面）

支付宝客服
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)2000691
（跳转支付宝客服页面）

支付宝生活
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)2000193
（跳转支付宝生活页面）

支付宝生活号
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)2000101
（跳转支付宝生活号页面）

支付宝记账
[alipayqr://platformapi/startapp?saId=](https://link.jianshu.com/?t=alipayqr://platformapi/startapp?saId=10000003)2000168
（跳转支付宝记账页面）



# 支付宝 二维码/转账码/生成方式,突破二维码生成数量的限制

> https://www.cnblogs.com/si812cn/p/8520335.html

Pxpay 个人收款开源项目: [https://www.ukafu.com/opensource.html](https://www.oschina.net/action/GoToLink?url=https%3A%2F%2Fwww.ukafu.com%2Fopensource.html)

支付宝收款的几种方式:

1. 通过xposed 设置金额/备注.然后可以得到一张二维码.这是传统的方式.
2. 通过支付宝的接口,自己拼接字符串.然后根据字符串生成一个二维码对于

对于传统方式.支付宝限制了一天二维码的生成数量.在这之间简直是好用得不要不要得.但是突然间支付宝爸爸说.你搞那么多二维码干啥.还备注...一天给你20张够不够?

于是乎一瞬间哀鸿遍野.有得同学有先见之明,生成好的二维码都存起来了.有存货,现在还可以拿出来用.但是没有存货的怎么办?

第二种方式,就出现了,转账码:

转账码也有多种途径

> [alipays://platformapi/startapp?appId=09999988&actionType=toAccount&goBack=NO&amount=1.00&userId=2088521328947850&memo=QQ_765858558](alipays://platformapi/startapp?appId=09999988&actionType=toAccount&goBack=NO&amount=1.00&userId=2088521328947850&memo=QQ_765858558)
>
> ![img](./alipay.assets\d195ccb40306f62ad4bf6d3dc4ea2596643.jpg)

> ![img](./alipay.assets\generate)不好意思.上面放的是红包.这个是对应的二维码,amount=金额,userId,嗯,这个需要获取支付宝唯一ID,memo,就是备注

自己拼接转账码,这种方式好用,方便,生成简单.老少皆宜,同嫂无欺,但是用户扫码之后.发现可编辑的.金额还可以改.备注也可以改.能改?用户别那么傻,改了你就会掉单.嗯程序员都是这么认为的.哪个**会去改啊.就这么用把.好上线吧.过了几天看看数据里,咦掉单了.咦怎么备注都是商品?卧槽.用户吧备注删了.!!! 这把轮到自己傻眼了吧

于是,最后一种方式,堪称完美

既能生成二维码,金额备注还是锁死的,用户还不能修改,一下子什么都解决了.

```javascript
<script>
function returnApp() {
    AlipayJSBridge.call("exitApp")
}

function ready(a) {
    window.AlipayJSBridge ? a && a() : document.addEventListener("AlipayJSBridgeReady", a, !1)
}
ready(function() {
    try {
        var a = {
            actionType: "scan",
            u: "2088521328947850",
            a: "200",
            m: "qq_765858558",
            biz_data: {
                s: "money",
                u: "2088521328947850",
                a: "200",
                m: "qq_765858558"
            }
        }
    } catch (b) {
        returnApp()
    }
    AlipayJSBridge.call("startApp", {
        appId: "20000123",
        param: a
    }, function(a) {})
});
document.addEventListener("resume", function(a) {
    returnApp()
});
</script>
```

这个拉起不好用了.道路总是再摸索中前进.组织需要你.

技术交流群:468737699,825751098