# 安装elasticsearch

1 环境
 linux 系统1.8.0_172  elasticsearch-6.6.1
2 下载
 https://www.elastic.co/
3 jdk安装
 略
4 安装ES

```
​	文件放到/usr/local/src
​	cd /usr/local
​	mkdir es	#创建文件夹
​	cd /usr/local/src
​	tar -zxvf elasticsearch-6.5.1.tar.gz -C /usr/local/es/  #解压
​	cd /usr/local/es
​	ls
​		elasticsearch-6.5.1
​	mkdir data  #创建ES数据文件和日志文件 
​	mkdir log  #创建ES数据文件和日志文件
​	groupadd elsearch #创建es用户组
​	useradd elsearch -g elsearch #创建es用户
​	chown -R elsearch:elsearch /usr/local/es #修改ES的根目录的权限
​	su - elsearch #切换用户 用root用户启动会报错 "can not run elasticsearch as root"
​	cd /usr/local/es/elasticsearch-6.5.1/
​	vi ./config/jvm.options #修改jvm内存因是在虚拟机中内存不足
​		-Xms2g      变成这样-------------->     -Xms512m
​		-Xmx2g      变成这样-------------->     -Xms512m
​	vim ./config/elasticsearch.yml #修改配置文件
​		path.data: /usr/local/es/data
​		path.logs: /usr/local/es/logs
​	./bin/elasticsearch
```
5 问题
解决问题【1】
 max file descriptors [4096] for elasticsearch process is too low, increase to at least [65536] 意思是说你的进程不够用了
解决方案： 
切到root 用户：进入到security目录下的limits.conf；执行命令 vim /etc/security/limits.conf 在文件的末尾添加下面的参数值：
```
    	* soft nofile 65536
        * hard nofile 131072
        * soft nproc 2048
        * hard nproc 4096
        或
        elsearch soft nofile 65536
        elsearch hard nofile 131072
        elsearch soft nproc 2048
        elsearch hard nproc 4096
```
前面的*(代表所有用户)符号必须带上，然后重新启动就可以了。
执行完成后可以使用命令 ulimit -n 查看进程数 

解决问题【2】
 max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]  需要修改系统变量的最大值了
解决方案：切换到root用户修改配置sysctl.conf  
vi /etc/sysctl.conf 
增加配置值：
vm.max_map_count=655360
执行命令 
sysctl -p   这样就可以了，然后重新启动ES服务 就可以了

解决问题【3】
Centos6不支持SecComp，而ES6默认bootstrap.system_call_filter为true
[es@elk2 elasticsearch-6.2.2]$ vim config/elasticsearch.yml
禁用：在elasticsearch.yml中配置bootstrap.system_call_filter为false，注意要在Memory下面: 
取消bootstrap.memory_lock的注释，添加bootstrap.system_call_filter 配置
​    bootstrap.memory_lock: false 
​    bootstrap.system_call_filter: false
再次启动es，成功启动

解决问题【4】
启动成功后，通过http://localhost:9200可以访问的到，但是http://ip:9200访问不到，怎么办呢?
解决方案
修改elasticsearch-2.3.3\config\elasticsearch.yml文件
放开如下配置，并写的本机ip
network.host：192.168.56.219

# 安装head 插件

2 head依赖于node 要先安装Node
​	1.到nodejs官网现在最新nodejs，官网下载地址：https://nodejs.org/en/download/ 
​	下载linux Binaries 64-bit的压缩包
​	解压&添加环境变量就OK了
​	node -v 验证安装
​	安装cnpm
​	npm install -g cnpm --registry=https://registry.npm.taobao.org
2 下载head安装包，下载地址：https://github.com/mobz/elasticsearch-head/archive/master.zip 这是接从git 上下载下来 ，然后上传到虚拟机上的；由于head 插件不能放在elasticsearch-5.6.3 文件夹里，head 插件需要单独放，单独去执行；所 以在elasticsearch-5.6.3 同级目录下解压了 head 插件；
​	进入解压目录
​	cnpm install #安装依赖
​	npm run start #启动 (在重启elasticsearch)
解决问题【1】
head连不上elasticsearch ，
解决方案 
​	重启elasticsearch
重启elasticsearch报中下错：

```
org.elasticsearch.bootstrap.StartupException: java.lang.IllegalArgumentException: property [elasticsearch.version] is missing for plugin [head]
    at org.elasticsearch.bootstrap.Elasticsearch.init(Elasticsearch.java:125) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.bootstrap.Elasticsearch.execute(Elasticsearch.java:112) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.cli.EnvironmentAwareCommand.execute(EnvironmentAwareCommand.java:86) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.cli.Command.mainWithoutErrorHandling(Command.java:124) ~[elasticsearch-cli-6.2.3.jar:6.2.3]
    at org.elasticsearch.cli.Command.main(Command.java:90) ~[elasticsearch-cli-6.2.3.jar:6.2.3]
    at org.elasticsearch.bootstrap.Elasticsearch.main(Elasticsearch.java:92) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.bootstrap.Elasticsearch.main(Elasticsearch.java:85) ~[elasticsearch-6.2.3.jar:6.2.3]
Caused by: java.lang.IllegalArgumentException: property [elasticsearch.version] is missing for plugin [head]
    at org.elasticsearch.plugins.PluginInfo.readFromProperties(PluginInfo.java:226) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.plugins.PluginInfo.readFromProperties(PluginInfo.java:184) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.bootstrap.Spawner.spawnNativePluginControllers(Spawner.java:75) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.bootstrap.Bootstrap.setup(Bootstrap.java:167) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.bootstrap.Bootstrap.init(Bootstrap.java:323) ~[elasticsearch-6.2.3.jar:6.2.3]
    at org.elasticsearch.bootstrap.Elasticsearch.init(Elasticsearch.java:121) ~[elasticsearch-6.2.3.jar:6.2.3]
```

解决方案 
​    当重启服务器之后发现依旧如上， 还是无法正常连接到elasticsearch服务，这是因为elasticsearch服务与elasticsearch-head之间可能存在跨越，修改elasticsearch配置即可，在elastichsearch.yml中添加如下命名即可：
```
​	#allow origin
​	http.cors.enabled: true
​	http.cors.allow-origin: "*"
```







